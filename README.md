# hilo


## install

### Linux

$ git clone https://gitlab.com/dao14/hilo.git

$ cd hilo

$ chmod +x install

$ ./install

## game

Hilo is a quick card game where the user guesses if
the next card will be higher / same or lower / same.

A key feature of the game is that losing a guess will
also end any previous streak. This creates an interesting
dynamic, forcing users to decide on cashout every turn,
incrementing slowly.

Writing the game in python proved to be relatively simple,
by using an abstraction of the card names as values.
This allowed 1 -> 13 to equal A -> K. A display_name is
created for the user to read.
